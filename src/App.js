import React, { Component } from 'react';
import './App.css';

import CryptoList from './CryptoList'

class App extends Component {
  render() {
    return (
      <div className="App">
        <CryptoList />
      </div>
    );
  }
}

export default App;
