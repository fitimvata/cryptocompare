import React from 'react'
import Coin from './Coin'

const URL = 'https://min-api.cryptocompare.com/data/all/coinlist'

function getCoinList(){
    return fetch(URL)
        .then(function(response) {
            return response.json()
        })
        .catch(function(error) {
            console.log(error)
        })
}


// const coinList = {
//     "42": {
//         "Id": "4321",
//         "Url": "/coins/42/overview",
//         "ImageUrl": "/media/12318415/42.png",
//         "Name": "42",
//         "Symbol": "42",
//         "CoinName": "42 Coin",
//         "FullName": "42 Coin (42)",
//         "Algorithm": "Scrypt",
//         "ProofType": "PoW/PoS",
//         "FullyPremined": "0",
//         "TotalCoinSupply": "42",
//         "PreMinedValue": "N/A",
//         "TotalCoinsFreeFloat": "N/A",
//         "SortOrder": "34",
//         "Sponsored": false,
//         "IsTrading": true
//     },
//     "123": {
//         "Id": "4321",
//         "Url": "/coins/42/overview",
//         "ImageUrl": "/media/12318415/42.png",
//         "Name": "42",
//         "Symbol": "42",
//         "CoinName": "42 Coin",
//         "FullName": "42 Coin (42)",
//         "Algorithm": "Scrypt",
//         "ProofType": "PoW/PoS",
//         "FullyPremined": "0",
//         "TotalCoinSupply": "42",
//         "PreMinedValue": "N/A",
//         "TotalCoinsFreeFloat": "N/A",
//         "SortOrder": "34",
//         "Sponsored": false,
//         "IsTrading": true
//     },
// }

class CryptoList extends React.Component {
    state = {
        currentPage: 0,
        perPage: 30,
        pages: []
    }

    componentDidMount() {
        getCoinList().then( (response) => {
            if(response) {
                const pages = this.formatList(response.Data)
                this.setState({
                    pages: pages
                })
            }
        })
    }

    formatList(coinList) {
        // return array of coinList keys;
        const keys = Object.keys(coinList)

        // map array of keys to array of coin objects
        const coins = keys.map(function(key) {
            return coinList[key]
        })

        const total = this.state.perPage
        const pages = []
        for(let i = 0; i < coins.length; i+=total) {
            const page = coins.slice(i, i + total)
            pages.push(page)
        }
        return pages
    }

    prevPage = () => {
        this.setState(prevState => {
            if(prevState.currentPage - 1 <= 0) {
                return {
                    currentPage: 0
                }
            }
            return {
                currentPage: prevState.currentPage - 1
            }
        })
    }

    nextPage = () => {
        this.setState(prevState => {
            if(prevState.currentPage + 1 >= (this.state.pages.length - 1)) {
                return {
                    currentPage: this.state.pages.length - 1
                }
            }
            return {
                currentPage: prevState.currentPage + 1
            }
        })
    }

    renderList() {
        const page = this.state.pages[this.state.currentPage] || []
        return page.map(function (coin) {
            return (
                <Coin key={coin.Id} coin={coin} />
            )
        })
    }

    render() {
        return (
            <div className="CryptoList">
                <div>
                    <button onClick={this.prevPage}>{'<<'}</button>
                    <button onClick={this.nextPage}>{'>>'}</button>
                    <span>
                        {this.state.currentPage + 1} / 
                        {this.state.pages.length}
                    </span>
                </div>
                {this.renderList()}
            </div>
        );
    }
}

export default CryptoList;