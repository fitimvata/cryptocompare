import React from 'react'

const baseUrl = "https://www.cryptocompare.com"


function Coin(props) {
    const coin = props.coin
    // css BEM convention
    return (
        <div className="Coin">
            <div  className="Coin__container">
                <img className="Coin__img" src={baseUrl + coin.ImageUrl} alt=""/>
                <div className="Coin__content">
                    <h2 className="Coin__title">
                        {coin.FullName}
                    </h2>
                </div>
            </div>  
        </div>
    )
}


export default Coin